# web-crawler-excercise

## Requirements

This project is based on [scala](https://www.scala-lang.org/) and uses [sbt](https://www.scala-sbt.org/) to build. Just download and install sbt and it will take care of downloading other dependencies. In order to run it you'll need to have Java Runtime installed on your machine.

## Compile

To build run at repository root

`sbt assembly`

This will create runnable jar in folder `target/scala-2.12/web-crawler.jar`

## Run

Use `java` command with `--jar` flag to start runnable jar

Here example invocation displaying help

```
> java -jar ./target/scala-2.12/web-crawler.jar --help
Usage: web-crawler.jar [options]
  --rootUrl <value>  rootUrl is the Url of page to start crawl at
  --limit <value>    limit is the upper limit of pages to be visited, default = 200
  --out              name of output json formatted file
  --help             display help message
```

Example running a crawl on https://en.wikipedia.org/wiki/Main_Page using a limit of 1000 pages and writing output to file `result.json`

`> java -jar ./target/scala-2.12/web-crawler.jar --rootUrl "https://en.wikipedia.org/wiki/Main_Page" --limit 1000 --out result.json`

**NOTE:** For convenient viweing of large JSON files you can use [jq](https://stedolan.github.io/jq/) tool

`> cat result.json | jq | less` will display nicely formatted output.