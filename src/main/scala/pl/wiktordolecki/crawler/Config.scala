package pl.wiktordolecki.crawler

import java.io.File

import scopt.OptionParser

case class Config
(
  rootUrl: String = "",
  limit: Int = Config.DefaultLimit,
  out: File = new File("")
)

object Config {
  val DefaultLimit: Int = 200

  val parser: OptionParser[Config] = new scopt.OptionParser[Config]("web-crawler.jar") {
    opt[String]("rootUrl").required().action((x, c) =>
      c.copy(rootUrl = x)).text("rootUrl is the Url of page to start crawl at")

    opt[Int]("limit").withFallback(() => DefaultLimit).action((x, c) =>
      c.copy(limit = x)).text("limit is the upper limit of pages to be visited, default = " + DefaultLimit)

    opt[File]("out").required().valueName("").
      action((x, c) => c.copy(out = x)).
      text("name of output json formatted file")

    help("help").text("display help message")
  }
}
