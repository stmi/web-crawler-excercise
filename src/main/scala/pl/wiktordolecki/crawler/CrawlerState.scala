package pl.wiktordolecki.crawler

import java.io.{BufferedWriter, File, FileWriter}
import java.net.URI

import scala.collection.mutable
import scala.util.Try
import net.liftweb.json._
import net.liftweb.json.Serialization.write

class CrawlerState(val rootUri: URI, val limit: Int) {
  val markedForVisit = new mutable.HashSet[URI]()
  markedForVisit.add(rootUri)

  val queue = new mutable.Queue[URI]
  queue.enqueue(rootUri)

  val pageMap = new mutable.HashMap[URI, List[String]]()
  val errors = new mutable.HashSet[String]()

  def writeToFile(file: File): Unit = {
    val prettyMap = Map(
      "root" -> rootUri.toString,
      "visitedPages" -> {
        pageMap map { case (uri, links) => Map (
          "url" -> uri.toString,
          "links" -> links
        )}
      }
    )

    val bw = new BufferedWriter(new FileWriter(file))

    write(prettyMap, bw)(DefaultFormats)

    bw.close()
  }
}

object CrawlerState {
  def fromConfig(config: Config): Try[CrawlerState] =
    Try[CrawlerState] {
      val rootUri = new URI(config.rootUrl)
      if (config.limit <= 0) throw new java.lang.IllegalArgumentException("Limit must be positive value")
      new CrawlerState(rootUri, config.limit)
    }
}