package pl.wiktordolecki.crawler

import java.net.{URI, URISyntaxException}

import org.jsoup.Jsoup

import scala.collection.JavaConverters._

object CrawlerWorker {
  def runCrawl(state: CrawlerState): Unit = {
    while (state.pageMap.size < state.limit && state.queue.nonEmpty) {
      val current = state.queue.dequeue()

      val doc = Jsoup.connect(current.toString).get
      val elems = doc.select("a").eachAttr("href").asScala

      val nextList = elems.flatMap(href => {
        try {
          Some(new URI(href))
        } catch {
          case _: URISyntaxException => state.errors.add(href)
            None
        }
      })
        .filter(uri => uri.getHost == state.rootUri.getHost || uri.getHost == null)
        .map(state.rootUri.resolve)
        .flatMap(uri => {
          try {
            // Drop query and fragment in this implementation
            Some(new URI(uri.getScheme, uri.getAuthority, uri.getPath, null, null))
          } catch {
            case _: URISyntaxException => state.errors.add(uri.toString)
              None
          }
        })
        .distinct

      nextList.foreach(uri => {
        if (!state.markedForVisit.contains(uri)) {
          state.markedForVisit.add(uri)
          state.queue.enqueue(uri)
        }
      })

      state.pageMap += current -> nextList.map(uri => uri.toString).toList
    }
  }
}
