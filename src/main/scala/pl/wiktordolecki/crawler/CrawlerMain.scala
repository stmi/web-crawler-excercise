package pl.wiktordolecki.crawler

import scala.util.{Failure, Success}

/**
  * Entry point for crawler execution.
  */
object CrawlerMain extends App {
  Config.parser.parse(args, Config()) match {
    case Some(config) =>
      CrawlerState.fromConfig(config) match {
        case Success(state) =>
          println("Web Crawler starting")
          CrawlerWorker.runCrawl(state)
          state.writeToFile(config.out)
        case Failure(exception) =>
          println("Error: starting parser using provided arguments not possible")
          println(exception)
      }

    case None =>
      Unit //Just do nothing, CLI library takes care of displaying info
  }
}
