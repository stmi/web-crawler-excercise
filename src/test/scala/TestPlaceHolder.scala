import org.scalatest.{FlatSpec, Matchers}

/**
  * This is just a placeholder test class that will generate minimum report
  * to be published by CI/CD job on GitLab
  */
class DummySpec extends FlatSpec with Matchers {

  "A DummySpec" should "assert that true is true" in {
    assert(true)
  }

  it should "match same things" in {
    "TEXT" shouldBe "TEXT"
  }

  ignore should "skip ignored tests which fail" in {
    assert(false)
  }
}
