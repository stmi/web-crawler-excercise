name := "web-crawler-exercise"
organization := "pl.wiktordolecki"
version := "1.0-SNAPSHOT"
licenses += "MIT" -> url("https://opensource.org/licenses/MIT")

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

scalaVersion := "2.12.7"
scalacOptions += "-target:jvm-1.8"

// Dependencies
libraryDependencies += "org.jsoup" % "jsoup" % "1.11.3" // HTML and DOM parser
libraryDependencies += "com.github.scopt" % "scopt_2.12" % "3.7.0" // CLI args parsing
libraryDependencies += "net.liftweb" %% "lift-json" % "3.3.0" // Json serialization

// Test scope
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test

// Configuration for assembly plugin
ThisBuild / assembly / assemblyJarName := "web-crawler.jar"
ThisBuild / assembly / mainClass := Some("pl.wiktordolecki.crawler.CrawlerMain")
